# README

Notes Application Front End

This is basic single page application to perform CRUD operations using jwt authorization. Notes can be created/edited/deleted and can be assigned to folders or multiple tags by making Ajax calls

### To run locally:

* Download/Clone both front end and back end of this application
* run commands
* please run this server at port 3003 because back end server would be running at port 3000

``` 
bundle install
rake db:migrate
rails s -b 0.0.0.0 -p 3003
```

Open http://localhost:3003/#!/


Back end of this application can be downloaded/cloned from:
https://bitbucket.org/EhiPassico/notes_app_api/src/master/