// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, or any plugin's
// vendor/assets/javascripts directory can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file. JavaScript code in this file should be added after the last require_* statement.
//
// Read Sprockets README (https://github.com/rails/sprockets#sprockets-directives) for details
// about supported directives.
//


//= require rails-ujs
//= require jquery.js
//= require activestorage


//= require angular
//= require angular-rails-templates.js.erb
//= require angular-sanitize
//= require angular-resource
//= require angular-bootstrap
//= require angular-block-ui
//= require angular-filter
//= require angular-route
//= require angular-ui-select
//= require angular-cookies
//= require angular-animate
//= require angularjs-toaster
//= require angular-block-ui

//= require_tree ./templates


//= require angular_app/config/notes_app_setup
//= require angular_app/config/notes_app_routes

//= require_tree ./angular_app/services
//= require_tree ./angular_app/controllers
