notes_app.service('ajaxService', ["$http", "$rootScope", function ($http, $rootScope) {
    var self = this

    self.back_end_url = $rootScope.back_end_url

    self.req = {
        method: 'GET',
        url: `${$rootScope.back_end_url}`,
        data: {},
        headers: {'Content-Type': 'application/json'}
    }

    self.folders = {
        index: {
            method: 'GET',
            url: `${$rootScope.back_end_url}/folders`,
            headers: {'Content-Type': 'application/json'}
        },
        create_folder: function (folder) {
            return ({
                method: 'POST',
                data: {'folder': folder},
                url: `${$rootScope.back_end_url}/folders`,
                headers: {'Content-Type': 'application/json'}
            })
        },

        update_folder: function (folder, folder_id) {
            return ({
                method: 'PUT',
                data: {'folder': folder},
                url: `${$rootScope.back_end_url}/folders/${folder_id}`,
                headers: {'Content-Type': 'application/json'}
            })
        }
    }

    self.tags = {
        index: {
            method: 'GET',
            url: `${$rootScope.back_end_url}/tags`,
            headers: {'Content-Type': 'application/json'}
        },
        create_tag: function (tag) {
            return ({
                method: 'POST',
                data: {'tag': tag},
                url: `${$rootScope.back_end_url}/tags`,
                headers: {'Content-Type': 'application/json'}
            })
        },
        update_tag: function (tag, tag_id) {
            return ({
                method: 'PUT',
                data: {'tag': tag},
                url: `${$rootScope.back_end_url}/tags/${tag_id}}`,
                headers: {'Content-Type': 'application/json'}
            })
        },
    }

    self.notes = {
        index: {
            method: 'GET',
            url: `${$rootScope.back_end_url}/notes`,
            headers: {'Content-Type': 'application/json'}
        },

        create_note: function (note) {
            return ({
                method: 'POST',
                data: {'note': note},
                url: `${$rootScope.back_end_url}/notes`,
                headers: {'Content-Type': 'application/json'}
            })
        },
        update_note: function (note, note_id) {
            return ({
                method: 'PUT',
                data: {'note': note},
                url: `${$rootScope.back_end_url}/notes/${note_id}`,
                headers: {'Content-Type': 'application/json'}
            })
        },

        delete_note: function (note_id) {
            return ({
                method: 'DELETE',
                data: {'note': note_id},
                url: `${$rootScope.back_end_url}/notes/${note_id}`,
                headers: {'Content-Type': 'application/json'}
            })
        },

        folder_notes: function (folder_id) {
            return ({
                method: 'Get',
                url: `${$rootScope.back_end_url}/folder_notes/${folder_id}`,
                headers: {'Content-Type': 'application/json'}
            })
        },
        
        tag_notes: function (tag_id) {
            return ({
                method: 'Get',
                url: `${$rootScope.back_end_url}/tag_notes/${tag_id}`,
                headers: {'Content-Type': 'application/json'}
            })
        },
    }

}])

notes_app.directive('setFocus', function () {
    return {
        link: function (scope, element, attrs) {
            element.bind('click', function () {
// alert(element.attr('id'));
                document.querySelector('#' + attrs.setFocus).focus();
            })
        }
    }
})