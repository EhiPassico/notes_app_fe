notes_app.service('jsServ', ['$filter', function ($filter) {
    var self = this

    self.is_object_in_objects = function (obj_property_for_search, property_value_to_search, objects) {
        //noinspection JSAnnotator
        var filtered_item = $filter('filter')(objects, {[obj_property_for_search]: property_value_to_search}, true)[0]
        var index = objects.indexOf(filtered_item)
        if (index >= 0) {
            return index
        } else {
            return -1
        }
    }

    self.replace_object_with_object_in_objects = function (obj_property_for_search, property_value_to_search, objects_array_to_search_in, object_to_assign) {
        //
        //noinspection JSAnnotator
        var filtered_item = $filter('filter')(objects_array_to_search_in, {[obj_property_for_search]: property_value_to_search}, true)[0]
        var index = objects_array_to_search_in.indexOf(filtered_item)
        if (index >= 0) {
            objects_array_to_search_in[index] = object_to_assign

        }
    }

}])