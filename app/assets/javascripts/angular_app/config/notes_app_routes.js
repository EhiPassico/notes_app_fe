notes_app.config(["$routeProvider", "$httpProvider", function ($routeProvider, $httpProvider) {
    // $routeProvider

    // $locationProvider.hashPrefix('');
    // $httpProvider
    //
    // $httpProvider.defaults.headers.common['X-CSRF-Token'] = $('meta[name=csrf-token]').attr('content')

    // $httpProvider.defaults.withCredentials = true

    $routeProvider

        .when('/', {
            templateUrl: 'home.html',
            controller: 'home_controller'
        })
        .when('/test', {
            templateUrl: 'test.html',
        })
        .when('/login', {
            templateUrl: 'login.html',
            controller: 'login_controller'
        })

        .when('/folders', {
            templateUrl: 'folders.html',
            controller: 'folders_controller'
        })

        .when('/tags', {
            templateUrl: 'tags.html',
            controller: 'tags_controller'
        })


    $httpProvider

}])

notes_app.factory('myHttpInterceptor', function ($q, $rootScope) {
    return {
        request: function (config) {

            config.headers['Authorization'] = $rootScope.current_user.token
            config.headers['Accept'] = 'application/json'
            return config;
        },
        response: function (response) {
            // do something on success
            if (response.headers()['content-type'] === "application/json; charset=utf-8") {
                if (typeof(response.data.logged_out != "undefined")) {
                    if (response.data.logged_out == true) {
                        $rootScope.logout();
                    }

                }
            }


            return response;
        }
        ,
        responseError: function (response) {
            // do something on error
            if (response.headers()['content-type'] === "application/json; charset=utf-8") {
                if (typeof(response.data.logged_out != "undefined")) {
                    if (response.data.logged_out == true) {
                        $rootScope.logout();
                    }

                }
            }
            return $q.reject(response);
        }
    };
})
;

notes_app.config(function ($httpProvider) {
    $httpProvider.interceptors.push('myHttpInterceptor');
});