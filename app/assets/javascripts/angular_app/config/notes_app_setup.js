notes_app = angular.module("notes_app", ['ngRoute', 'ngSanitize', 'ngResource', 'templates', 'ui.bootstrap', 'ngCookies', 'ngAnimate', 'toaster', 'ui.select','blockUI']).run(["$rootScope", "$window", "$cookies", "$location", "toaster", function ($rootScope, $window, $cookies, $location, toaster) {

    toaster.pop('info', 'title', 'note')

    $rootScope.back_end_url = 'http://' + $location.$$host + ':3000'

    // console.log('$location',$location.$$absUrl);
    // console.log($location.$$host + ':3000')

    // $rootScope.back_end_url = 'http://142.93.157.17:3000'
    // $rootScope.back_end_url = 'http://localhost:3000'


    $rootScope.current_user = {
        authenticated: false,
        token: null
    }

    $rootScope.vm = {}
    $rootScope.logout = function () {

        $rootScope.current_user.token = null
        $window.localStorage.setItem("notes_app_token", null)
        toaster.pop('info', "logged out")
        $location.path('/login')
    }


    if ($rootScope.current_user.token == null) {
        var lc_token = $window.localStorage.getItem("notes_app_token")
        if (lc_token == "null") {
            $rootScope.current_user.token = null
        } else {
            $rootScope.current_user.token = lc_token
        }
    }


}])

notes_app.run(function ($rootScope, $location, toaster, $http) {
    $rootScope.$on("$locationChangeStart", function (event, next, current) {

        // if user not logged in
        if ($rootScope.current_user.token == null && $location.path() != '/login') {
            $location.path('/login')
            //alert login
            toaster.pop('info', "Please login")


        }
        // if user already logged in
        else if ($rootScope.current_user.token != null && $location.path() == '/login') {
            $location.path('/')
        }
    });

    // $httpProvider.responseInterceptors.push(function ($q, dependency1, dependency2) {
    //     return function (promise) {
    //         return promise.then(function (response) {
    //             // do something on success
    //             console.log("helloooo");
    //             return response;
    //         }, function (response) {
    //             // do something on error
    //             if (canRecover(response)) {
    //                 return responseOrNewPromise
    //             }
    //             return $q.reject(response);
    //         });
    //     }
    // });


});


//sample $http
// $http(req).then(function (response) {
// }, function (error) {
// })