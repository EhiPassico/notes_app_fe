notes_app.controller('tags_controller', ["$scope", "$rootScope", "$http", "$location", "toaster", "$filter", "ajaxService", function ($scope, $rootScope, $http, $location, toaster, $filter, ajaxService) {
    $scope.cv = {
        show_input: 'search',
        new_tag: {},
    }

    $http(ajaxService.tags.index).then(function (response) {
        if (response.data.request_status == 'ok') {
            $scope.cv.tags = response.data.tags
        } else {
        }
    })


    $scope.create_tag = function () {
        var req = ajaxService.tags.create_tag($scope.cv.new_tag)

        $http(req).then(function (response) {
            if (response.data.request_status == 'ok') {
                $scope.cv.tags.push(response.data.tag)
                toaster.pop('success', 'tag created')
            } else {
                toaster.pop('error', 'tag not created')
            }
        }, function (error) {
        })
    }

    $scope.edit_tag = function (tag) {
        $scope.cv.edit_tag = angular.copy(tag)
        $scope.cv.show_input = 'edit_tag'
        $scope.cv.search_input = ''
    }

    $scope.update_tag = function () {

        var req = ajaxService.tags.update_tag($scope.cv.edit_tag, $scope.cv.edit_tag.id)


        $http(req).then(function (response) {
            if (response.data.request_status == 'ok') {
                var response_tag = response.data.tag
                var found = $filter('filter')($scope.cv.tags, {id: response_tag.id}, true)
                if (found.length) {
                    $scope.cv.tags[$scope.cv.tags.indexOf(found[0])] = response_tag;
                    toaster.pop('success', 'Updated')
                }

            } else {
                toaster.pop('error', 'Not Updated')
            }
        }, function (error) {
        })
    }

}])
