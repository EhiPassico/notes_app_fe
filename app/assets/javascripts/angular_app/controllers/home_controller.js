notes_app.controller('home_controller', ["$scope", "$http", "$cookies", "$window", "$rootScope", "toaster", "ajaxService", "$filter", "jsServ", function ($scope, $http, $cookies, $window, $rootScope, toaster, ajaxService, $filter, jsServ) {


    $scope.cv = {
        show_notes: true,
        show_new_note: false,
        show_edit_note: false,
        new_note: {},
        notes_heading: '',
        new_note_selected_tags: [],
        edit_note_selected_tags: [],
        tags_side_bar_is_open: true,
        notes_side_bar_is_open: true
    }

    $scope.form = {}

    $http.get(`${$scope.back_end_url}/is_logged_in.json`).then(function (response) {
        $scope.response = response
    })

    // fetch user folders
    $scope.fetch_user_folders = function () {
        $http(ajaxService.folders.index).then(function (response) {
            if (response.data.request_status == 'ok') {
                $scope.cv.folders = response.data.folders
            } else {
            }
        })
    }
    $scope.fetch_user_folders()


    // fetch user tags
    $scope.fetch_user_tags = function () {
        $http(ajaxService.tags.index).then(function (response) {
            if (response.data.request_status == 'ok') {
                $scope.cv.tags = response.data.tags
                $scope.cv.notes_heading = 'All Notes'
            } else {
                toaster.pop('error', 'Error')
            }
        })
    }
    $scope.fetch_user_tags()

    // fetch user notes
    $scope.fetch_all_user_notes = function () {
        $http(ajaxService.notes.index).then(function (response) {
            if (response.data.request_status == 'ok') {
                $scope.cv.notes = response.data.notes
                $scope.cv.current_folder_view = null
                $scope.cv.notes_heading = 'All Notes'
                $scope.reset_selected_folder()
                //tempdel
                // $scope.edit_note($scope.cv.notes[0])
                //tempdel

            } else {
                toaster.pop('error', 'Error')
            }
        })
    }
    $scope.fetch_all_user_notes();

    //fetch inbox notes
    // need to make new ajax call. For now use filter
    $scope.fetch_inbox_notes = function(){
        $http(ajaxService.notes.index).then(function (response) {
            if (response.data.request_status == 'ok') {
                $scope.cv.notes = response.data.notes
                $scope.cv.current_folder_view = null
                $scope.cv.notes_heading = 'Inbox Notes'
                $scope.reset_selected_folder()
                //tempdel
                // $scope.edit_note($scope.cv.notes[0])
                //tempdel
                $scope.cv.notes = $filter('filter')($scope.cv.notes, {folder_id: null}, true)


            } else {
                toaster.pop('error', 'Error')
            }
        })

    }


    $scope.set_show_block = function (option) {
        $scope.cv.show_new_note = false
        if (option == 'notes') {
            $scope.cv.show_notes = true
            $scope.cv.show_new_note = false
            $scope.cv.show_edit_note = false
        } else if (option == 'new_note') {
            // $scope.cv.new_note = {}
            // $scope.cv.show_new_note = true
            // $scope.cv.show_notes = false

            $scope.new_note()

        } else if (option == 'edit_note') {
            $scope.cv.show_edit_note = true
            $scope.cv.show_new_note = false
            $scope.cv.show_notes = false

        }
    }

    $scope.new_note = function () {
        $scope.cv.new_note = {}
        $scope.cv.show_new_note = true
        $scope.cv.show_notes = false

        $scope.cv.new_note_selected_tags = []

        if (typeof $scope.cv.current_folder_view != "undefined") {
            var found = $filter('filter')($scope.cv.folders, {id: $scope.cv.current_folder_view}, true)
            if (found.length) {
                $scope.cv.new_note_selected_folder = found[0]
            }
        }
    }

    $scope.set_new_note_tags = function (note, selected_tags) {
        var tag_ids = []

        if (selected_tags.length > 0) {
            selected_tags.forEach(function (tag) {
                tag_ids.push(tag['id'])
            })
            note.note_tag_ids = tag_ids
        }
    }


    $scope.create_new_note = function () {
        if (typeof $scope.cv.new_note_selected_folder != "undefined") {
            $scope.cv.new_note.folder_id = $scope.cv.new_note_selected_folder.id
        }


        $scope.set_new_note_tags($scope.cv.new_note, $scope.cv.new_note_selected_tags)

        $http(ajaxService.notes.create_note($scope.cv.new_note)).then(function (response) {
            if (response.data.request_status == 'ok') {
                $scope.cv.notes.push(response.data.note)
                $scope.set_show_block('notes')
                toaster.pop('success', 'Note Created!', $scope.cv.new_note.title)

                // fetch totals after new note creation
                $scope.fetch_user_folders();
                $scope.fetch_user_tags();
            } else {
                toaster.pop('error', 'Error')
            }
        })
    }

    $scope.cancel_form_create = function () {
        $scope.cv.show_new_note = false
        $scope.cv.show_notes = true
    }

    $scope.edit_note = function (note) {
        $scope.cv.edit_note = note
        $scope.set_show_block('edit_note')
        $scope.set_selected_note(note)

        var found = $filter('filter')($scope.cv.folders, {id: $scope.cv.edit_note.folder_id}, true)
        if (found.length) {
            $scope.cv.edit_note_selected_folder = found[0]
        }

        $scope.cv.edit_note_selected_tags = []

        //    set edit tags
        if (note.tag_ids != null) {
            JSON.parse(note.tag_ids).forEach(function (tag_id) {
                var index = jsServ.is_object_in_objects('id', tag_id, $scope.cv.tags)
                $scope.cv.edit_note_selected_tags.push($scope.cv.tags[index])
            })
        }


    }

    $scope.set_update_note_tags = function (note, selected_tags) {
        var tag_ids = []

        if (selected_tags.length > 0) {
            selected_tags.forEach(function (tag) {
                tag_ids.push(tag['id'])
            })

            if (note.tags == null) {
                note.note_tag_ids = tag_ids
            } else if (JSON.parse(note.tag_ids).sort() != tag_ids.sort()) {
                $scope.tags_modified_in_edit_note = true
                note.note_tag_ids = tag_ids
            }
        }
    }


    $scope.update_note = function (option) {
        var folder_id_before_update = $scope.cv.edit_note.folder_id


        if (typeof $scope.cv.edit_note_selected_folder != "undefined") {
            if ($scope.cv.edit_note_selected_folder == null) {
                $scope.cv.edit_note.folder_id = null
            } else {
                $scope.cv.edit_note.folder_id = $scope.cv.edit_note_selected_folder.id
            }


        }

        $scope.set_update_note_tags($scope.cv.edit_note, $scope.cv.edit_note_selected_tags)



        $http(ajaxService.notes.update_note($scope.cv.edit_note, $scope.cv.edit_note.id)).then(function (response) {
                if (response.data.request_status == 'ok') {
                    toaster.clear()
                    toaster.pop('success', 'Note Updated!', $scope.cv.edit_note.title)

                    // update note in notes
                    var found = $filter('filter')($scope.cv.notes, {id: $scope.cv.edit_note.id}, true)
                    if (found.length) {
                        var index = $scope.cv.notes.indexOf(found[0])
                        $scope.cv.notes[index] = response.data.note
                        $scope.cv.edit_note = response.data.note
                        $scope.set_selected_note($scope.cv.edit_note)

                    }

                    // update folder count if folder is changed
                    var folder_id_after_update = $scope.cv.edit_note.folder_id

                    if (folder_id_before_update != folder_id_after_update) {
                        //    decrement the count for before folder id
                        var index = jsServ.is_object_in_objects('id', folder_id_before_update, $scope.cv.folders)
                        if (index > -1) {
                            $scope.cv.folders[index].notes_count = $scope.cv.folders[index].notes_count - 1
                        }


                        //    increment the count for after folder id
                        var index = jsServ.is_object_in_objects('id', folder_id_after_update, $scope.cv.folders)
                        if (index > -1) {
                            $scope.cv.folders[index].notes_count = $scope.cv.folders[index].notes_count + 1
                        }
                    }
                    //refresh folder count
                    $scope.fetch_user_folders();
                    $scope.fetch_user_tags();
                    // update tags count
                    if ($scope.tags_modified_in_edit_note == true) {
                        $scope.fetch_user_tags()
                    }

                    if (option == 'save') {
                        $scope.cv.editNoteForm.$setPristine()
                    } else {
                        $scope.set_show_block('notes')
                    }


                }
                else {
                    toaster.pop('error', 'Error')
                }
            }
        )
    }


    $scope.cancel_note_update = function () {
        $scope.cv.show_edit_note = false
        $scope.cv.show_notes = true
    }

    $scope.delete_note = function () {
        if (confirm("Sure to delete?")) {
            $http(ajaxService.notes.delete_note($scope.cv.edit_note.id)).then(function (response) {
                if (response.data.request_status == 'ok') {

                    var found = $filter('filter')($scope.cv.notes, {id: $scope.cv.edit_note.id}, true)
                    if (found.length) {
                        var index = $scope.cv.notes.indexOf(found[0])

                        if (index != -1) {
                            $scope.cv.notes.splice(index, 1)
                        }
                    }
                    toaster.pop('success', 'Note Deleted!', $scope.cv.edit_note.title)

                    $scope.fetch_user_tags()
                    $scope.fetch_user_folders()

                    $scope.set_show_block('notes')


                } else {
                    toaster.pop('error', 'Error')
                }
            }, function (error) {
                toaster.pop('error', 'Error')
            })
        }


    }

    $scope.reset_selected_folder = function () {
        $scope.cv.folders.forEach(function (f) {
            f.is_selected = false
        })
    }


    $scope.set_selected_folder = function (folder) {
        $scope.reset_selected_folder()
        folder.is_selected = true
    }

    $scope.get_folder_notes = function (folder) {
        $scope.reset_selected_tag()

        var folder_id = folder.id
        $scope.set_selected_folder(folder)
        $http(ajaxService.notes.folder_notes(folder_id)).then(function (response) {
            if (response.data.request_status == 'ok') {
                $scope.set_show_block('notes')
                $scope.cv.notes = response.data.notes
                $scope.cv.current_folder_view = folder_id
                $scope.cv.notes_heading = `Folder: ${folder.name}`
            }
        }, function (error) {
        })
    }


    $scope.reset_selected_note = function () {
        $scope.cv.notes.forEach(function (f) {
            f.is_selected = false
        })
    }


    $scope.set_selected_note = function (note) {
        $scope.reset_selected_note()
        note.is_selected = true
    }

//    tag_notes

    $scope.reset_selected_tag = function () {
        $scope.cv.tags.forEach(function (t) {
            t.is_selected = false
        })
    }

    $scope.set_selected_tag = function (tag) {
        $scope.reset_selected_tag()
        tag.is_selected = true
    }

    $scope.get_tag_notes = function (tag) {
        $scope.reset_selected_folder()

        var tag_id = tag.id
        $scope.set_selected_tag(tag)
        $http(ajaxService.notes.tag_notes(tag_id)).then(function (response) {
            if (response.data.request_status == 'ok') {
                $scope.set_show_block('notes')
                $scope.cv.notes = response.data.notes
                $scope.cv.current_tag_view = tag_id
                $scope.cv.notes_heading = `tag: ${tag.name}`
            }
        }, function (error) {
        })
    }


}])

