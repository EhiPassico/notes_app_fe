notes_app.controller('login_controller', ["$scope", "$rootScope", "$http", "$location", "$window", "toaster", function ($scope, $rootScope, $http, $location, $window, toaster) {


    $scope.cv = {
        show_signup_form: false,
        email: null,
        password: null
    }


    $scope.sign_in_user = function () {

        var req = {
            method: 'POST',
            url: `${$rootScope.back_end_url}/login`,
            data: {email: $scope.cv.email, password: $scope.cv.password},
            headers: {'Content-Type': 'application/json'}
        }

        if ($scope.cv.email != null && $scope.cv.password != null) {

            $http(req).then(function (response) {
                if (response.data.req_status == 'success') {

                    $rootScope.current_user.token = response.data.token
                    $window.localStorage.setItem("notes_app_token", response.data.token)
                    toaster.pop('success', 'Signed in successfully!')
                    $location.path('/')

                } else {
                    $scope.cv.password = null
                    toaster.pop('error', 'Invalid email/password')
                }
            })
        } else {
            toaster.pop('warning', 'Please enter email/password')
        }
    }

    $scope.sign_up_user = function () {
        var req = {
            method: 'POST',
            url: `${$rootScope.back_end_url}/signup`,
            data: {user: {email: $scope.cv.email, password: $scope.cv.password}},
            headers: {'Content-Type': 'application/json'}
        }


        if ($scope.cv.email != null && $scope.cv.password != null) {

            $http(req).then(function (response) {
                if (response.data.req_status == 'success') {

                    $rootScope.current_user.token = response.data.token
                    $window.localStorage.setItem("notes_app_token", response.data.token)
                    toaster.pop('success', 'Signup Successful!')
                    $location.path('/')

                } else {
                    $scope.cv.password = null
                    toaster.pop('error', 'Invalid email/password')
                }
            }, function (error) {
                toaster.pop('error', error.data.toast_message)
            })
        } else {
            toaster.pop('warning', 'Please enter email/password')
        }

    }


    //
    // $http({method: 'GET', url: 'www.google.com/someapi', headers: {
    //     'Authorization': 'Basic QWxhZGRpbjpvcGVuIHNlc2FtZQ=='}
    // });
    // Or with the shortcut method:
    //
    //     $http.get('www.google.com/someapi', {
    //         headers: {'Authorization': 'Basic QWxhZGRpbjpvcGVuIHNlc2FtZQ=='}
    //     });
}
])