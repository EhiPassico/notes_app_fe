notes_app.controller('folders_controller', ["$scope", "$rootScope", "$http", "$location", "toaster", "$filter", "ajaxService", function ($scope, $rootScope, $http, $location, toaster, $filter, ajaxService) {
    $scope.cv = {
        show_input: 'search',
        new_folder: {},
    }

    $http(ajaxService.folders.index).then(function (response) {
        if (response.data.request_status == 'ok') {
            $scope.cv.folders = response.data.folders
        } else {
        }
    })


    $scope.create_folder = function () {
        var req = ajaxService.folders.create_folder($scope.cv.new_folder)

        $http(req).then(function (response) {
            if (response.data.request_status == 'ok') {
                $scope.cv.new_folder.name = null
                $scope.cv.folders.push(response.data.folder)
                toaster.pop('success', 'Created')

            } else {
                toaster.pop('error', 'Folder not created')
            }
        }, function (error) {
        })
    }

    $scope.edit_folder = function (folder) {
        $scope.cv.edit_folder = angular.copy(folder)
        $scope.cv.show_input = 'edit_folder'
    }

    $scope.update_folder = function () {

        var req = ajaxService.folders.update_folder($scope.cv.edit_folder, $scope.cv.edit_folder.id)

        $http(req).then(function (response) {
            if (response.data.request_status == 'ok') {
                var response_folder = response.data.folder
                var found = $filter('filter')($scope.cv.folders, {id: response_folder.id}, true)
                if (found.length) {
                    $scope.cv.folders[$scope.cv.folders.indexOf(found[0])] = response_folder;
                    toaster.pop('success', 'Updated')
                }

            } else {
                toaster.pop('error', 'Not Updated')
            }
        }, function (error) {
        })
    }

}])
